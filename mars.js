
const turnLeft = (position) => {
	switch (position.orientation) {
		case 'E':
			position.orientation = 'N';
		break;
		case 'S':
			position.orientation = 'E';
		break;
		case 'W':
			position.orientation = 'S';
		break;
		case 'N':
			position.orientation = 'W';
		break;
	}
	return position;
};

const turnRight = (position) => {
	switch (position.orientation) {
		case 'E':
			position.orientation = 'S';
		break;
		case 'S':
			position.orientation = 'W';
		break;
		case 'W':
			position.orientation = 'N';
		break;
		case 'N':
			position.orientation = 'E';
		break;
	}
	return position;
};

const moveForward = (position) => {
	switch (position.orientation) {
		case 'E':
			position.x = ++position.x;
		break;
		case 'S':
			position.y = --position.y;
		break;
		case 'W':
			position.x = --position.x;
		break;
		case 'N':
			position.y = ++position.y;
		break;
	}
	return position;
};

const willMoveOffMars = (scent, position) => {
	if (scent.lenght == 0) return false;
	let found = false;
	for(let i = 0; i < scent.length; i++) {
	    if (scent[i].x == position.x && scent[i].y == position.y && scent[i].orientation == position.orientation ) {
	        found = true;
	        break;
	    }
	}
	return found;
}

const registerScent = (scent, position) => {
	scent.push({
		x: position.x,
		y: position.y,
		orientation: position.orientation
	});
}

const finalPosition = (scent, position, mars) => {

	if (mars.x > 50 || mars.y > 50) return 'The maximum value for any coordinate is 50.';

	let instructions = position.instruction.split('');
	for (let i = 0; i <= instructions.length; i++) {
		switch (instructions[i]) {
			case 'L':
				position = turnLeft(position);
			break;
			case 'R':
				position = turnRight(position);
			break;
			case 'F':
				let currentPosition = {...position};
				if ( ! willMoveOffMars(scent, position)) {

					position = moveForward(position);

					if (position.x > mars.x || position.y > mars.y || position.x < 0 || position.y < 0) {
						position.lost = 'LOST';
						registerScent(scent, currentPosition)
						continue;
					}
				}
			break;
		}
	}
	return `${position.x} ${position.y} ${position.orientation} ${position.lost ? position.lost : ''}`;
};

const mars = {
	x: 5,
	y: 3
};

var scent = [];

var position = {
	x: 1,
	y: 1,
	orientation: 'E',
	instruction: 'RFRFRFRF'
};
console.log(finalPosition(scent, position, mars));

position = {
	x: 3,
	y: 2,
	orientation: 'N',
	instruction: 'FRRFLLFFRRFLL'
};
console.log(finalPosition(scent, position, mars));

position = {
	x: 0,
	y: 3,
	orientation: 'W',
	instruction: 'LLFFFLFLFL'
};
console.log(finalPosition(scent, position, mars));


